#!/bin/sh
# ========================================================================
# $File: run.sh $
# $Date: 2019-10-17 10:20:42 $
# $Revision: $
# $Creator: Jen-Chieh Shen $
# $Notice: See LICENSE.txt for modification and distribution information
#                   Copyright © 2019 by Shen, Jen-Chieh $
# ========================================================================

cd ..

node main.js
