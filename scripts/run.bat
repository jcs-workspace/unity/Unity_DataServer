@echo off
:: ========================================================================
:: $File: run.bat $
:: $Date: 2019-10-16 11:53:34 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2019 by Shen, Jen-Chieh $
:: ========================================================================

cd ..


node main.js
