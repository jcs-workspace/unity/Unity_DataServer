/**
 * $File: config.js $
 * $Date: 2019-10-16 11:48:33 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2019 by Shen, Jen-Chieh $
 */

"use strict";

/* Application Info (App資訊) */
const APP_NAME = "[Your Game] Serv";

/* Debug Info (除錯資訊) */
const DEBUG = true;                     /* IMPORTANT: 發布記得關閉 */
const isDebug = function () { if (DEBUG) return 'debug'; else return 'release'; };

/* Network Info (連線資訊) */
const PORT = 2323;         /* Server Port [2323] */

/* Database Info (資料庫資訊) */
const SAVE_PATH = './data/';
const SAVE_EXT = '.jcs';

//------------------ Exports modules ------------------//

// NOTE: 以下請照順序擺放,感謝合作!

module.exports.APP_NAME = APP_NAME;

module.exports.DEBUG = DEBUG;
module.exports.isDebug = isDebug;

module.exports.PORT = PORT;

module.exports.SAVE_PATH = SAVE_PATH;
module.exports.SAVE_EXT = SAVE_EXT;
