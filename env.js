/**
 * $File: env.js $
 * $Date: 2019-10-16 13:33:05 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2019 by Shen, Jen-Chieh $
 */

"use strict";

const fs = require('fs');
const path = require("path");
const log4js = require('log4js');

/* Config */
global.CONFIG = require('./config');

/* Path */
global.PROJECT_PATH = path.resolve(".");

/* Util */
global.FS = fs;

global.LOGGER = log4js.getLogger();
LOGGER.level = CONFIG.isDebug();

global.UTIL = require(PROJECT_PATH + '/util');
