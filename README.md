[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Release](https://img.shields.io/github/tag/jcs090218/Unity_DataServer.svg?label=release&logo=github)](https://github.com/jcs090218/Unity_DataServer/releases/latest)

# Unity DataServer
> Unity device unique identifier data server.

[![Build Status](https://travis-ci.com/jcs090218/Unity_DataServer.svg?branch=master)](https://travis-ci.com/jcs090218/Unity_DataServer)

This project is to design to solve the game data missing after 
the game is uninstall from mobile device. You could implement 
your In-App Purchase (IAP) system/server with this data server 
to have your game server remebering the purchase history. The 
benefit could be the users purchasing with IAP could be 
cross-platform. Leading the Unity Developers would not need 
to concern about each IAP system's implementation to the 
game client. Google Play, iOS App Store, etc all in one.

*P.S. The problem is that the data may be lost after the user 
has changed their mobile device.*

## How to use?

Here are something you need to know before you use this data 
server. The server is base on [Node.js](https://nodejs.org/en/), 
please first install Node.js. This server design to use Unity's 
scripting API; it provided the device unique indentifier (DUID) 
to store data base on the device ID. 


* [SystemInfo.deviceUniqueIdentifier](https://docs.unity3d.com/ScriptReference/SystemInfo-deviceUniqueIdentifier.html)
in Unity References
* [Node.js](https://nodejs.org/en/) environment and [JavaScript](https://en.wikipedia.org/wiki/JavaScript) (For extensibility)

## Dependencies

* [express](https://github.com/expressjs/express)
* [body-parser](https://www.npmjs.com/package/body-parser)
* [log4js](https://www.npmjs.com/package/log4js)

## Setup the Server

These are commands that you need to setup and run the server. 
The more you understand Node.js the more you know about this. 


```sh
# clone this repository
$ git clone https://github.com/jcs090218/Unity_DataServer.git

# change directory to project root directory.
$ cd ./Unity_DataServer

# install needed npm packages
$ npm install

# run the data server
$ node main.js
```

You should see something like this as output from the last command. 
This mean the data server is up and running successfully.

```sh
Enabling data server, [Your Game] Serv:::
Active port: 2323
Done activation :::
```

Any changes can be set in [config.js](./config.js) file.

## Example Code (Client)

These code demonstrate what you need from C# in Unity Engine.

First you need to defined the saved data structure that will 
holds all your game data.

```cs
/// <summary>
/// Game data structure. (遊戲存檔資料結構)
/// </summary>
[System.Serializable]
public class GameData
{
    // You must have this to store as `unique identifier`.
    // 你必須要有這個來儲存`唯一標識符`.
    public string uid;
}
```

The file below demonstrate how you use to call the API for saving.

```cs
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Use example. (使用範例)
/// </summary>
public class UseExample : MonoBehaviour
{
    private void Awake()
    {
        // Start Coroutine (開始協程)
        {
            // Save Data once. (儲存資料)
            StartCoroutine(SendSaveData("localhost:2323/save/"));
            // Load Data once. (載入資料)
            StartCoroutine(SendSaveData("localhost:2323/load/"));
        }
    }

    private IEnumerator SendSaveData(string url)
    {
        GameData data = new GameData();
        data.uid = SystemInfo.deviceUniqueIdentifier;

        UnityWebRequest req = new UnityWebRequest(url, "POST");

        /* Register Handler (註冊控制器) */
        {
            // Upload Handler (上傳控制器)
            byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(JsonUtility.ToJson(data));
            req.uploadHandler = new UploadHandlerRaw(jsonToSend);

            // Download Handler (下載控制器)
            req.downloadHandler = new DownloadHandlerBuffer();

            // Set Header (設置標頭)
            req.SetRequestHeader("Content-Type", "application/json");
        }

        /*  Yield for response. (暫擱協程直到收到回覆) */
        yield return req.SendWebRequest();

        if (req.isNetworkError)
            Debug.Log("Error: " + req.error);
        else
        {
            // Print out the response message. (印出回傳訊息)
            Debug.Log("Received: " + req.downloadHandler.text);
        }

        // Clean it. (清理IO)
        req.Dispose();
    }
}
```

## Todo List

These server aren't a perfect save/load game data server.

- [ ] Log system for log checking.
- [ ] Error handling, backup for all game data files.

## License

MIT © [Jen-Chieh Shen](http://www.jcs-profile.com/)
