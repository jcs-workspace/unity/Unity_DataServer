/**
 * $File: util.js $
 * $Date: 2019-10-16 13:39:49 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2019 by Shen, Jen-Chieh $
 */

"use strict";

/**
 * Check the save data exists/valid.
 * @param { string } udid : Unqiue device identifier.
 */
function checkSaveData(udid) {
  let path = CONFIG.SAVE_PATH + udid + CONFIG.SAVE_EXT;
  return FS.existsSync(path);
}

//------------------ Exports modules ------------------//

module.exports.checkSaveData = checkSaveData;
