/**
 * $File: main.js $
 * $Date: 2019-10-16 11:42:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2019 by Shen, Jen-Chieh $
 */

"use strict";

const env = require('./env');

const fs = require('fs');

const express = require('express'), app = express();
const bodyParser = require('body-parser');


/* Register all request. */
app.use(bodyParser.json());
app.post('/save/', saveData);
app.post('/load/', loadData);


app.set('port', process.env.PORT || CONFIG.PORT);

// Start listening..
const server = app.listen(app.get('port'), function () {
  console.log("Enabling data server, %s:::", CONFIG.APP_NAME);
  console.log("Active port: %s", app.get('port'));
  console.log("Done activation :::");
});

//=============== Functions ========================//

/**
 * Request for save data route.
 * @param { typename } req : Request.
 * @param { typename } res : Response.
 * @param { typename } next : Next route interface.
 */
function saveData(req, res, next) {
  LOGGER.debug("Incoming save request...");

  let uid = req.body.uid;
  let content = req.body;
  let path = CONFIG.SAVE_PATH + uid + CONFIG.SAVE_EXT;

  if (uid === undefined) {
    LOGGER.debug("Undefined save request.");
    res.send("Failed to save data");
    return;
  }

  FS.writeFile(path, JSON.stringify(content), 'utf8', function (err) {
    if (err) {
      LOGGER.debug("An error occured while writing JSON Object to File.");
      return console.log(err);
    }
    LOGGER.debug("JSON file has been saved.");
    return 0;
  });

  res.send("Success to save data");
}

/**
 * Load the game data and send it.
 * @param { typename } req : Request.
 * @param { typename } res : Response.
 * @param { typename } next : Next route interface.
 */
function loadData(req, res, next) {
  LOGGER.debug("Incoming load request...");

  let uid = req.body.uid;
  let path = CONFIG.SAVE_PATH + uid + CONFIG.SAVE_EXT;

  if (!UTIL.checkSaveData(uid, req.body)) {
    let errMsg = "Record doesn't exists";
    LOGGER.debug(errMsg);
    res.send(errMsg);
    return;
  }

  let obj = JSON.parse(fs.readFileSync(path, 'utf8'));
  res.send(obj);
}
